//
//  ProfileController.swift
//  TrailHop
//
//  Created by iosdev on 6.5.2021.
//

import Foundation
import UIKit
import MapKit

class ProfileController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var cellID = "cellID"
    var items : [Item] = [Item]()
    var filteredData: [Item] = [Item]()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(ItemCell.self, forCellReuseIdentifier: cellID)
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        filteredData = items
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        STrailValues.co2 = String(STrailValues.information[indexPath.item].co2)
        STrailValues.distance = String(STrailValues.information[indexPath.item].distance)
        STrailValues.speed = STrailValues.information[indexPath.item].speed
        STrailValues.polyline = String(STrailValues.information[indexPath.item].polyline)
        STrailValues.type = String(STrailValues.information[indexPath.item].activity())
        STrailValues.time = String((STrailValues.information[indexPath.item].timestampEnd - STrailValues.information[indexPath.item].timestampStart) / 1000)
        performSegue(withIdentifier: "SingleTrail", sender: self)
    
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! ItemCell
        let currentLastItem = filteredData[indexPath.row]
        cell.item = currentLastItem
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        150
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (STrailValues.favorites.isEmpty){
            print("Empty")
        } else {
        dataToCells()
        }
    }

    //MARK: Search Bar Config
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filteredData = []
        
        if (searchBar.text == nil || searchBar.text == ""){
            self.filteredData = self.items
        } else {
            for item in items {
                
                if item.type.lowercased().contains(searchText.lowercased()){
                    
                    filteredData.append(item)
                }
            }
        }
        self.tableView.reloadData()
    }
    
    func dataToCells () {
        print(STrailValues.favorites)
        for index in 0...STrailValues.favorites.count - 1 {
            if (STrailValues.favorites[index].favoriteInProfile == false) {
                STrailValues.favorites[index].favoriteInProfile = true
                items.append(Item(map: STrailValues.favorites[index].map, type: STrailValues.favorites[index].type, time: STrailValues.favorites[index].time, distance: STrailValues.favorites[index].distance, favorite: true, favoriteInProfile: true))
            } else {
                print("Already in profile")
            }
        
            searchBar(searchBar, textDidChange: "")
        }
        
    }
    
    //MARK: Profile Picture Config
        @IBAction func didTapButton(){
            
            let vc = UIImagePickerController()
            vc.sourceType = .photoLibrary
            vc.delegate = self
            vc.allowsEditing = true
            present(vc, animated: true)
            
        }
    
}
extension ProfileController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")]as? UIImage {
            imageView.image = image
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }

}
