//
//  ViewController.swift
//  TrailHop
//
//  Created by iosdev on 13.4.2021.
//aaa

import UIKit
import MOPRIMTmdSdk
import CoreLocation
import CoreMotion
import MapKit
import Polyline
class ViewController: UIViewController, CLLocationManagerDelegate, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, MKMapViewDelegate {
    let locationManager = CLLocationManager()
    //var myData = [""]
    var filteredData: [Item] = [Item]()
    @IBOutlet var tableView: UITableView!
    var items : [Item] = [Item]()
    let cellID = "cellID"
    @IBOutlet weak var searchBar: UISearchBar!
    private var activities: [TMDActivity] = []
    private var currentDate: Date = Date()
    var mapView = MKMapView()
    var coordinates: [CLLocationCoordinate2D]!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //Test
        tableView.register(ItemCell.self, forCellReuseIdentifier: cellID)
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        mapView.delegate = self
        filteredData = items
        TMD.start()
        
        TMDCloudApi.generateSyntheticData(withOriginLocation: CLLocation.init(latitude: 60, longitude: 24.938949), destination: CLLocation.init(latitude: 90, longitude: 24.939049), requestType: TMDSyntheticRequestType.bicycle, hereApiKey: "gbQoL3WzflRncLCfxFdNb2Fy7mLp53dWvlnNAdMI9zc")
        
        
        TMDCloudApi.generateSyntheticData(withOriginLocation: CLLocation.init(latitude: 50, longitude: 30), destination: CLLocation.init(latitude: 70, longitude: 25), stopTimestamp: 0, requestType: TMDSyntheticRequestType.car, hereApiKey: "gbQoL3WzflRncLCfxFdNb2Fy7mLp53dWvlnNAdMI9zc")
 
        
        TMDCloudApi.fetchMetadata().continueWith { (task) -> Any? in
            DispatchQueue.main.async {
                // Execute your UI related code on the main thread
                if let error = task.error {
                    NSLog("fetchMetadata Error: %@", error.localizedDescription)
                }
                else if let metadata = task.result {
                    NSLog("fetchMetadata result: %@", metadata)
                }
                //return nil;
            }
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let DateTime = Date(timeIntervalSinceNow: -670740)
        print("time", DateTime)
        TMDCloudApi.fetchData(withStart: 0000000000000, withEnd: 9999999999999).continueWith { (task) -> Any? in
            DispatchQueue.main.async {
                // Execute your UI related code on the main thread
                if let error = task.error {
                    NSLog("fetchData Error: %@", error.localizedDescription)
                }
                else if let data = task.result {
                    for activity in (data as! [TMDActivity]) {
                        self.activities.append(activity)
                    }
                    NSLog("fetchData result: %@", data)
                    print("anything", self.activities.count)
                    //print( self.activities[3].activity())
                    STrailValues.information = self.activities
                    
                    self.dataToCell(self.activities)
                }
                
            }
           
        }
    }
    func askLocationPermissions() {
            self.locationManager.delegate = self
            self.locationManager.requestAlwaysAuthorization()
        }
        
        func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
            if #available(iOS 14.0, *) {
                let preciseLocationAuthorized = (manager.accuracyAuthorization == .fullAccuracy)
                if preciseLocationAuthorized == false {
                    manager.requestTemporaryFullAccuracyAuthorization(withPurposeKey: "tmd.AccurateLocationPurpose")
                    // Note that this will only ask for TEMPORARY precise location.
                    // You should make sure to ask your user to keep the Precise Location turned on in the Settings.
                }
            } else {
                // No need to ask for precise location before iOS 14
            }
        }
    
    let motionActivityManager = CMMotionActivityManager()
    func askMotionPermissions() {
        if CMMotionActivityManager.isActivityAvailable() {
            self.motionActivityManager.startActivityUpdates(to: OperationQueue.main) { (motion) in
                print("received motion activity")
                self.motionActivityManager.stopActivityUpdates()
            }
        }
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        STrailValues.co2 = String(activities[indexPath.item].co2)
        STrailValues.distance = String(activities[indexPath.item].distance)
        STrailValues.speed = activities[indexPath.item].speed
        STrailValues.polyline = String(activities[indexPath.item].polyline)
        STrailValues.type = String(activities[indexPath.item].activity())
        STrailValues.time = String((activities[indexPath.item].timestampEnd - activities[indexPath.item].timestampStart) / 1000)
        performSegue(withIdentifier: "SingleTrail", sender: self)
        print("indexpath", indexPath.item)
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! ItemCell
        let currentLastItem = filteredData[indexPath.row]
        STrailValues.index = indexPath.item
        cell.item = currentLastItem
        return cell
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        150
    }
    //MARK: Search Bar Config
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filteredData = []
        
        if (searchBar.text == nil || searchBar.text == ""){
            self.filteredData = self.items
        } else {
            for item in items {
                
                if item.type.lowercased().contains(searchText.lowercased()){
                    
                    filteredData.append(item)
                }
            }
        }
        self.tableView.reloadData()
    }
    
    func dataToCell(_ information: [TMDActivity]) {
        for index in 0...information.count - 1 {
            coordinates = decodePolyline(information[index].polyline)
            items.append(Item(map: mapView, type: "Type: " + String(information[index].activity()), time: "Time: " + String((information[index].timestampEnd - information[index].timestampStart) / 1000) + " seconds", distance: "Distance: " + String(information[index].distance) + " meters", favorite: false, favoriteInProfile: false))
            searchBar(searchBar, textDidChange: "")
            
        }
    }
    
    }


