//
//  ItemCell.swift
//  TrailHop
//
//  Created by iosdev on 29.4.2021.
//

import UIKit
import MapKit
import Polyline

protocol ItemCellDelegate {
    func addFavorite(cell: ItemCell)
}

class ItemCell : UITableViewCell {
    var delegate: ItemCellDelegate?
    
    var item : Item? {
        didSet {
            itemMap.self = item?.map as! UIView
            itemType.text = item?.type
            itemTime.text = item?.time
            itemDistance.text = item?.distance
        }
    }
    private let itemDistance : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.textAlignment = .left
        lbl.numberOfLines = 0
        return lbl
    }()
    
    private let itemType : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.textAlignment = .left
        return lbl
    }()
    
    private let itemTime : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.textAlignment = .left
        return lbl
    }()
    
    private let itemFavorite : UIButton = {
        let btn = UIButton(type: .custom)
        btn.setImage(UIImage(systemName: "star"), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFill
        return btn
    }()
    
    @objc func dostuff() {
        if (item?.favorite == false) {
            item?.favorite = true
        STrailValues.favorites.append(item!)
            
        } else {
            print("Already added")
        }
       
    }
    
    private var itemMap : UIView = {
        let mapView = MKMapView()
        mapView.contentMode = .scaleAspectFit
        mapView.clipsToBounds = true
        var index = STrailValues.index
        print("index", index)
        print("poly", STrailValues.information[index].polyline)
        var coordinates: [CLLocationCoordinate2D]! = decodePolyline(STrailValues.information[index].polyline)
        var points: [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
        var location = CLLocationCoordinate2D(latitude: 1, longitude: 1)
        var region = MKCoordinateRegion(center: location, latitudinalMeters: 3000, longitudinalMeters: 5000)
        
        
        for coordinate in coordinates {
            points.append(coordinate)
        }
        for index in 0...points.count - 1{
            location = CLLocationCoordinate2D(latitude: points[index].latitude, longitude: points[index].longitude)
            region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: points[index].latitude, longitude: points[index].longitude), latitudinalMeters: 3000, longitudinalMeters: 5000)
        }
       let Polyline = MKPolyline(coordinates: points, count: points.count)
        
        mapView.setRegion(region, animated: true)
        mapView.addOverlay(Polyline)
        
        func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            
            if overlay is MKPolyline {
                polylineRenderer.strokeColor = UIColor.blue
                polylineRenderer.lineWidth = 5
            }
            return polylineRenderer
        }
        
        return mapView
    }()
    
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(itemMap)
        addSubview(itemType)
        addSubview(itemTime)
        addSubview(itemDistance)
        addSubview(itemFavorite)
        
        contentView.isUserInteractionEnabled = true
        
        itemMap.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: nil, paddingTop: 5, paddingLeft: 5, paddingBottom: 5, paddingRight: 0, width: 90, height: 0, enableInsets: false)
         itemType.anchor(top: topAnchor, left: itemMap.rightAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: frame.size.width, height: 0, enableInsets: false)
        itemTime.anchor(top: itemType.bottomAnchor, left: itemMap.rightAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: frame.size.width, height: 0, enableInsets: false)
        itemDistance.anchor(top: itemTime.bottomAnchor, left: itemMap.rightAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: frame.size.width, height: 0, enableInsets: false)
 
         
        let stackView = UIStackView(arrangedSubviews: [itemFavorite])
         stackView.distribution = .equalSpacing
         stackView.axis = .horizontal
         stackView.spacing = 1
         addSubview(stackView)
        stackView.anchor(top: itemDistance.bottomAnchor, left: itemMap.rightAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 15, paddingLeft: 5, paddingBottom: 15, paddingRight: 10, width: 0, height: 30, enableInsets: false)
        
        itemFavorite.addTarget(self, action: #selector(dostuff), for: .touchUpInside)
 
}
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("wwoowow")
    }
}
