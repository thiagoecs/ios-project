//
//  StartViewController.swift
//  TrailHop
//
//  Created by iosdev on 4.5.2021.
//

import UIKit
import MapKit
import CoreLocation
import MOPRIMTmdSdk

class StartViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var startButton: UIButton!
    
    var timer:Timer = Timer()
    var count:Int = 0
    var timerCounting:Bool = false
    
    var buttonCounter = 1
    private let locationManager:CLLocationManager = CLLocationManager()
    var coordinates: [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
    
    
    /*
     override func viewDidLoad() {
     super.viewDidLoad()
     // Do any additional setup after loading the view.
     
     locationManager.requestWhenInUseAuthorization()
     locationManager.desiredAccuracy = kCLLocationAccuracyBest
     locationManager.distanceFilter = kCLDistanceFilterNone
     locationManager.startUpdatingLocation()
     
     mapView.showsUserLocation = true
     }
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        func startStopRequestAuthorization(manager: CLLocationManager, status: CLAuthorizationStatus){
            switch status {
            case .notDetermined:
                manager.requestWhenInUseAuthorization()
            case .restricted, .denied:
                manager.startUpdatingLocation()
                manager.delegate = nil
            case .authorizedWhenInUse, .authorizedAlways:
                print("enabling location services")
                manager.delegate = self
                self.startLocationUpdates()
            }
        }
        self.startLocationUpdates()
        
        startButton.setTitleColor(UIColor.green, for: .normal)
        
        mapView.delegate = self
        mapView.showsUserLocation = true
        mapView.setUserTrackingMode(MKUserTrackingMode.follow, animated: true)
        
        //let location = CLLocation(latitude: 60.16, longitude: 24.93)
        if let userLocation = locationManager.location?.coordinate {
            let coordinateRegion = MKCoordinateRegion(center: userLocation, latitudinalMeters: 200, longitudinalMeters: 200)
            mapView.setRegion(coordinateRegion, animated: true)
        }
        
        //let test: [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
        
        /*
         let polyline = MKPolyline(coordinates: [locations[0].coordinate], count: locations.count)
         mapView.addOverlay(polyline)
         */
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation){
        print("user location did update")
        
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer{
        if (overlay is MKPolyline) {
            let polyLineRender = MKPolylineRenderer(overlay: overlay)
            polyLineRender.strokeColor = UIColor.green
            polyLineRender.lineWidth = 4
            
            return polyLineRender
        }
        return MKPolylineRenderer()
    }
    
    func startLocationUpdates(){
        if CLLocationManager.locationServicesEnabled(){
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = kCLDistanceFilterNone
            locationManager.startUpdatingLocation()
        } else {
            print("location services not available")
        }
    }
    
    //func locationManager(_ manager: CLLocation, didUpdateLocations locations:){}
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus){
        startStopRequestAuthorization(manager: manager, status: status)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        //print("location did update\(locations[0].coordinate)")
        print("all locations\(locations)")
        mapView.setCenter(locations[0].coordinate, animated: true)
        
        for location in locations{
            coordinates.append(location.coordinate)
        }
        
        var polyline = MKPolyline(coordinates: &coordinates, count: coordinates.count)
        
        if (buttonCounter == 0){
            mapView.addOverlay(polyline)
        }
        /*
         let polyline = MKPolyline(coordinates: [locations[0].coordinate], count: locations.count)
         mapView.addOverlay(polyline)
         */
        
        //var polyline = MKPolyline(coordinates: locations, count: locations.count)
    }
    
    
    func startStopRequestAuthorization(manager: CLLocationManager, status: CLAuthorizationStatus){
        switch status {
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
        case .restricted, .denied:
            manager.startUpdatingLocation()
            manager.delegate = nil
        case .authorizedWhenInUse, .authorizedAlways:
            print("enabling location services")
            manager.delegate = self
            self.startLocationUpdates()
        }
    }
    //MARK: StartButtonConfig
    @IBAction func didTapStart(_ sender: UIButton){
        
        if(timerCounting){
            timerCounting = false
            timer.invalidate()
        } else {
            timerCounting = true
        }
        buttonCounter += 1 // Increment buttonCounter variable by 1
        
        switch buttonCounter { // Perform a switch statement to get value of buttonCounter and run code in matching case
        
        case 1:
            sender.setTitle("Start", for: .normal)
            timerCounting = false
            startButton.setTitleColor(UIColor.green, for: .normal)
            self.count = 0
            self.timer.invalidate()
            self.timerLabel.text = self.makeTimeString(hours: 0, minutes: 0, seconds: 0)
            
            print("stop button has been pressed")
            TMD.stop()
            
        case 2:
            sender.setTitle("Stop", for: .normal)
            timerCounting = true
            startButton.setTitleColor(#colorLiteral(red: 0.6717393994, green: 0.08416283876, blue: 0, alpha: 1), for: .normal)
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerCounter), userInfo: nil, repeats: true)
            
            print("start button has been pressed")
            TMD.start()
            buttonCounter = 0
            
            
        default:
            print("Unable to change button title text.")
            
        }
        
    }
    @objc func timerCounter() -> Void {
        count = count + 1
        let time = secondsToHoursMinutesSeconds(seconds: count)
        let timeString = makeTimeString(hours: time.0, minutes: time.1, seconds: time.2)
        timerLabel.text = timeString
    }
    func secondsToHoursMinutesSeconds(seconds: Int) -> (Int, Int, Int) {
        return ((seconds / 3600), ((seconds % 3600) / 60), ((seconds % 3600) % 60))
    }
    func makeTimeString(hours: Int, minutes: Int, seconds: Int) -> String {
        var timeString = ""
        timeString += String(format: "%02d", hours)
        timeString += ":"
        timeString += String(format: "%02d", minutes)
        timeString += ":"
        timeString += String(format: "%02d", seconds)
        return timeString
        
    }
    
}
