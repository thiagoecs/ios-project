//
//  SingleItemCell.swift
//  TrailHop
//
//  Created by iosdev on 4.5.2021.
//

import UIKit

class SingleItemCell: UITableViewCell {
    var singleItem : SingleItem? {
        didSet {
            singleItemCo2.text = singleItem?.co2
            singleItemDistance.text = singleItem?.distance
            singleItemSpeed.text = singleItem?.speed
            singleItemType.text = singleItem?.type
            singleItemTime.text = singleItem?.time
        }
    }
    
    private let singleItemCo2 : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.boldSystemFont(ofSize: 25)
        lbl.textAlignment = .left
        lbl.numberOfLines = 0
        return lbl
    }()
    
    private let singleItemDistance : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.boldSystemFont(ofSize: 25)
        lbl.textAlignment = .left
        return lbl
    }()
    
    private let singleItemSpeed : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.boldSystemFont(ofSize: 25)
        lbl.textAlignment = .left
        return lbl
    }()
    
    private let singleItemType : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.boldSystemFont(ofSize: 25)
        lbl.textAlignment = .left
        lbl.numberOfLines = 0
        return lbl
    }()
    
    private let singleItemTime : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.boldSystemFont(ofSize: 25)
        lbl.textAlignment = .left
        lbl.numberOfLines = 0
        return lbl
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(singleItemCo2)
        addSubview(singleItemDistance)
        addSubview(singleItemSpeed)
        addSubview(singleItemType)
        addSubview(singleItemTime)
        backgroundColor = UIColor.white
        singleItemCo2.anchor(top: singleItemSpeed.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: frame.size.width / 1, height: 0, enableInsets: false)
        singleItemDistance.anchor(top: singleItemTime.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: frame.size.width / 1, height: 0, enableInsets: false)
        singleItemSpeed.anchor(top: singleItemDistance.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: frame.size.width / 1, height: 0, enableInsets: false)
        singleItemType.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: frame.size.width / 1, height: 0, enableInsets: false)
        singleItemTime.anchor(top: singleItemType.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: frame.size.width / 1, height: 0, enableInsets: false)
}
    
    override func updateConstraints() {
        super.updateConstraints()
        
        layoutMargins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("wwoowow")
    }
}


