//
//  SingleTrailController.swift
//  TrailHop
//
//  Created by iosdev on 3.5.2021.
//

import UIKit
import MapKit
import Polyline
import MOPRIMTmdSdk

class SingleTrailController: UIViewController, UITableViewDelegate, UITableViewDataSource, MKMapViewDelegate {
    
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var startButton: UIButton!
    
    @IBOutlet weak var mapView: MKMapView!
    
    var timer:Timer = Timer()
    var count:Int = 0
    var timerCounting:Bool = false
    
    var buttonCounter = 1
    
    var singleItems : [SingleItem] = [SingleItem]()
    let cellIDS = "cellID"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.layer.cornerRadius = 20
        tableView.register(SingleItemCell.self, forCellReuseIdentifier: cellIDS)
        tableView.delegate = self
        tableView.dataSource = self
        singleItems.append(SingleItem(co2: "CO₂:           " + String(STrailValues.co2) + " grams", distance: "Distance: " + String(STrailValues.distance) + " meters", speed: "Speed:      " + String(format: "%.5f", STrailValues.speed) + " km/h", type: "Type:         " + String(STrailValues.type), time: "Time:         " + String(STrailValues.time) + " seconds"))
        mapView.delegate = self
        
        createPolyline()
        mapView.showsUserLocation = true
        startButton.setTitleColor(UIColor.green, for: .normal)
    }
    
    func createPolyline() {
        let coordinates: [CLLocationCoordinate2D]! = decodePolyline(String(STrailValues.polyline))
        var points: [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
        var location = CLLocationCoordinate2D(latitude: 1, longitude: 1)
        var region = MKCoordinateRegion(center: location, latitudinalMeters: 3000, longitudinalMeters: 5000)
        for coordinate in coordinates {
            points.append(coordinate)
        }
        for index in 0...points.count - 1{
            location = CLLocationCoordinate2D(latitude: points[index].latitude, longitude: points[index].longitude)
            region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: points[index].latitude, longitude: points[index].longitude), latitudinalMeters: 3000, longitudinalMeters: 5000)
        }
        print(region)
       let Polyline = MKPolyline(coordinates: points, count: points.count)
        
        mapView.setRegion(region, animated: true)
        mapView.addOverlay(Polyline)
        }
    
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return singleItems.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIDS, for: indexPath) as! SingleItemCell
            let currentLastItem = singleItems[indexPath.row]
            cell.singleItem = currentLastItem
            return cell
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            400
        }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        
        if overlay is MKPolyline {
            polylineRenderer.strokeColor = UIColor.blue
            polylineRenderer.lineWidth = 5
        }
        return polylineRenderer
    }
    
    @IBAction func didTapStart(_ sender: UIButton) {
        if(timerCounting){
            timerCounting = false
            timer.invalidate()
        } else {
            timerCounting = true
        }
        buttonCounter += 1 // Increment buttonCounter variable by 1
        
        switch buttonCounter { // Perform a switch statement to get value of buttonCounter and run code in matching case
        
        case 1:
            sender.setTitle("Start", for: .normal)
            timerCounting = false
            startButton.setTitleColor(UIColor.green, for: .normal)
            self.count = 0
            self.timer.invalidate()
            self.timerLabel.text = self.makeTimeString(hours: 0, minutes: 0, seconds: 0)
            
            print("stop button has been pressed")
            TMD.stop()
            
        case 2:
            sender.setTitle("Stop", for: .normal)
            timerCounting = true
            startButton.setTitleColor(#colorLiteral(red: 0.6717393994, green: 0.08416283876, blue: 0, alpha: 1), for: .normal)
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerCounter), userInfo: nil, repeats: true)
            
            print("start button has been pressed")
            TMD.start()
            buttonCounter = 0
            
            
        default:
            print("Unable to change button title text.")
            
        }
        
    }
    @objc func timerCounter() -> Void {
        count = count + 1
        let time = secondsToHoursMinutesSeconds(seconds: count)
        let timeString = makeTimeString(hours: time.0, minutes: time.1, seconds: time.2)
        timerLabel.text = timeString
    }
    func secondsToHoursMinutesSeconds(seconds: Int) -> (Int, Int, Int) {
        return ((seconds / 3600), ((seconds % 3600) / 60), ((seconds % 3600) % 60))
    }
    func makeTimeString(hours: Int, minutes: Int, seconds: Int) -> String {
        var timeString = ""
        timeString += String(format: "%02d", hours)
        timeString += ":"
        timeString += String(format: "%02d", minutes)
        timeString += ":"
        timeString += String(format: "%02d", seconds)
        return timeString
        
    }
}
