//
//  SingleItem.swift
//  TrailHop
//
//  Created by iosdev on 4.5.2021.
//

import UIKit

struct SingleItem {
    var co2: String
    var distance: String
    var speed: String
    var type: String
    var time: String
}
