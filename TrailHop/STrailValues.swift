//
//  STrailValues.swift
//  TrailHop
//
//  Created by iosdev on 4.5.2021.
//

import Foundation
import UIKit
import MOPRIMTmdSdk
import MapKit
struct STrailValues {
    static var co2 = ""
    static var distance = ""
    static var speed = 0.0
    static var type = ""
    static var time = ""
    static var polyline = ""
    static var information : [TMDActivity] = []
    static var index = 0
    static var favorites: [Item] = []
}
