//
//  Item.swift
//  TrailHop
//
//  Created by iosdev on 29.4.2021.
//

import UIKit
import MapKit

struct Item {
    var map : MKMapView!
    var type : String
    var time: String
    var distance: String
    var favorite: Bool
    var favoriteInProfile: Bool
}
